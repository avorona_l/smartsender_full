$.validator.addMethod("validaria", function(value, element) {
    value = $('#phone').attr("aria-valid");
    return value == "true";
});



$(function () {

    var telInput = $("#phone"),
        errorMsg = $("#error-msg"),
        validMsg = $("#valid-msg");

// initialise plugin
    telInput.intlTelInput({
        utilsScript: "../libs/intlTelInput/utils.js"
    });

    var reset = function() {
        telInput.removeClass("error");
        errorMsg.addClass("hide");
        validMsg.addClass("hide");
    };

// on blur: validate
    telInput.blur(function() {
        reset();
        if ($.trim(telInput.val())) {
            if (telInput.intlTelInput("isValidNumber")) {
                validMsg.removeClass("hide");
            } else {
                telInput.addClass("error");
                errorMsg.removeClass("hide");
            }
        }
    });

// on keyup / change flag: reset
    telInput.on("keyup change", reset);

});

$(function () {
    $("form[name='first-form-enter']").validate({
        /*debug: true,*/
        rules:{
            firstname: {
                required: true,
                lettersonly:true
            },
            lastname: {
                required: true,
                lettersonly:true
            },
            phone:{
                required:true,
                validaria:true
            },
            zip:{
                required: true,
                digits: true,
                rangelength: [3, 3]
            }
        }
    });
});


$(function () {
    /*$("#creditcard").mask("9999 9999 9999 9999",{placeholder:"XXXX-XXXX-XXXX-XXXX"});*/
    $("form[name='main-form']").validate({
        rules: {
            editname: {
                required:true,
                lettersonly:true
            },
            cvv: {
                required: true,
                digits: true,
                rangelength: [3, 3]
            },
            editaddress: {
                required: true,
                lettersonly:true
            },
            editcity: {
                required: true,
                lettersonly:true
            },
            phone:{
                required: true,
                validaria:true
            },
            creditcard: {
                required: true,
                digits: true/*,
                 creditcard: true,
                 creditcardtypes: true*/
                /*rangelength: [16, 16]*/
            },
            addresssecond:{
                required: true,
                lettersonly:true
            },
            editstate:{
                required: true,
                lettersonly:true
            },
            zipcode:{
                required: true,
                digits: true,
                rangelength: [3, 3]
            },
            editcompany: {
                required: true,
                lettersonly:true
            },
            vatnumber: {
                required: false,
                digits: true
            }
        }
    })
});

$('.form-enter a.close-popup').on('click', function () {
    var validator = $("form[name='main-form']").validate();
    validator.resetForm();
});
