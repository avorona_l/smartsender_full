$(document).ready(function(){

    $('#btn-view-api').on('click', function () {
        $('#view-api').val($(this).val());
    });

    /********* GENERATE NEW API KEY ***************/
    function generateRandSrt() {
        var result = '';
        var words = '0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
        for (var i = 0; i < 16; ++i) {
            result += words.charAt(Math.floor(Math.random()*words.length));
        }
        return result;
    }

    $('body').on('click', '.btn-confirm-generate', function () {
        $('div.password-text').text($('#repeat-new-pass').val());
        $('#view-api').val(generateRandSrt());
    });


    /********* COPY API KEY ***************/
    $('.btn-copy-api-key').on('click', function () {
        $('#view-api').select();
        document.execCommand('copy');
    });


    /********* SHOW HAMBURGER  ***************/
    $('.hamburger').on('click', function() {
        var nav = $('.nav-wrap, .header-content');
        if (nav.hasClass('active')) {
            nav.removeClass('active');
        } else {
            nav.addClass('active');
        }
    });
    $('.header-content').click(function(){
        $('.nav-wrap, .header-content').removeClass('active');
    });


    $(".btn-invite,.btn-change-email, .btn-change-password, .btn-google-authentication," +
        ".btn-use-authentication-google, .btn-totp, .btn-change-api," +
        ".btn-change-payment, .btn-deactivate, .btn-apple-code").fancybox({
        'hideOnContentClick': true,
        'scrolling': 'visible',
        'closeBtn': true,
        'fixed': true,
        'autoCenter': true
    });

        /**************** NEXT STEP AUTH *************/
    $(".btn-next-step-authentication").fancybox({
        'hideOnContentClick': true,
        'scrolling': 'visible',
        'closeBtn': true,
        'fixed': true,
        'autoCenter': true
    });

    $(".btn-confirm-change-payment").fancybox({
        'hideOnContentClick': true,
        'scrolling': 'visible',
        'closeBtn': true,
        'fixed': false,
        'autoCenter': false
    });

    /********* SAVE SUBSCRIBE AREA ***********/
    $('body').on('click', '.save-subscribe', function(){
        $('input#choice-promo-code').val($('#select-subscribe option:selected').text());
    });

    /*.btn-confirm-change,   .btn-confirm-change-pass,*/
    $('.close-popup, .btn-confirm-generate, .btn-confirm-deactivate, .btn-save-new-payment,'+
		'.btn-send-invate, .save-subscribe').on('click', function(){
        $(this).closest('.fancybox-skin').find('.fancybox-close').trigger('click');
    });

    /********* COPY REFERRAL LINK ***************/
    $('#copy-btn-link').on('click', function () {
        console.log(123);
        $('#copy-link').select();
        document.execCommand('copy');
    });

    $('#country-select-inform, #country-select').countrySelect();


    /********** CHANGE EMAIL *********/
    $('body').on('click', '.btn-confirm-change', function(){
        var emailRegex = new RegExp(/.+@.+\..+/i);
        var emailAddress = $('#new-email');
        var validmail = emailRegex.test(emailAddress.val());
        if(!validmail){
            emailAddress.addClass('error');
        }
        else{
            emailAddress.removeClass('error');
            $('div.email-address-text').text(emailAddress.val());
            $('#old-email').val('');
            emailAddress.val('');
            $.fancybox.close();
        }
    });


    /*********** CHANGE PASSWORD BTN *************/
    $('body').on('click', '.btn-confirm-change-pass', function () {
        var newPass = $('#new-pass');
        var repeatPass = $('#repeat-new-pass');
        if(newPass.val() == ''){
            newPass.addClass('error');
        }
        else if(repeatPass.val() == ''){
            newPass.removeClass('error');
            repeatPass.addClass('error');
        }
        else if(newPass.val() == repeatPass.val()){
            newPass.removeClass('error');
            repeatPass.removeClass('error');
            $('div.password-text').text(repeatPass.val());
            $('#current-pass').val('');
            newPass.val('');
            repeatPass.val('');
            $.fancybox.close();
        }
        else{
            return false;
        }
    });


    $('#btn-summ').on('click', function () {
       if($('#summ').val() == ''){
           $('#summ').addClass('error')
       }
        else if($('body').on('click', function (e) {
               $(e.target).closest("#btn-summ").length||
               $('#summ').removeClass('error');
       }));
       else{
            $('#summ').removeClass('error')
        }
    });


    $('.btn-view-code').on('click', function () {
        if($('#connect-setup').val() == ''){
            $('#connect-setup').addClass('error')
        }
        else{
            $('#connect-setup').removeClass('error');
            $('#connect-google-popup').hide();
        }
    });

    $('#btn-connect-enter-code').on('click', function () {
        if($('#connect-enter-code').val() == ''){
            $('#connect-enter-code').addClass('error')
        }
        else{
            $('#connect-enter-code').removeClass('error');
            $('#next-step-authentication').hide();
        }
    });

    $('.btn-check-input').on('click', function () {
        if($('#check-input').val() == ''){
            $('#check-input').addClass('error')
        }
        else{
            $('#check-input').removeClass('error');
            /*$('#next-step-authentication').hide();*/
        }
    });

    /********* TEXTAREA TEST ***********/
    $('.btn-send-invate').on('click', function () {
        console.log(123);
        if($('#invate-textarea').val() == ''){
            $('#invate-textarea').addClass('error')
        }
        else{
            $('#invate-textarea').removeClass('error');
            $('#invite-friend-by-email').hide();
        }
    });


    function checkPass (){
        var newPass = $('#new-pass').val();
        var repeatPass = $('#repeat-new-pass').val();

        if (newPass !== repeatPass){
            newPass.addClass('error');
            repeatPass.addClass('error');
        }
        else {
            newPass.removeClass('error');
            repeatPass.removeClass('error');
            $('div.password-text').text($(repeatPass).val());
            $('#current-pass').val('');
            newPass.val('');
            repeatPass.val('');
        }
    }


    $('body').on('click', '.btn-confirm-deactivate', function () {
        console.log(123);
        $('div.output-credit-cart, div.output-method, div.output-account').html('');
    });
	
	
    $('body').on('click', '.btn-save-new-payment', function () {
		$('div.password-text').text($('#repeat-new-pass').val());
        $('#current-pass').val('');
        $('#new-pass').val('');
        $('#repeat-new-pass').val('');
    });

    /******************** SLIDER FOR TABLE AND PAGER ****************/
    var e=$(".first-number-page"),
        n=$(".last-number-page"),
        o=$(".slider-table");
    o.on("init reInit afterChange",function(t,o,c){
        var i=(c||0)+1;
        e.text(i);
        n.text(o.slideCount)});
        o.slick({slide:"div.slider-table-item",
            autoplay:!1,
            centerMode:!0,
            centerPadding:0,
            slidesToShow:1,
            dots:!1,
            prevArrow:$(".prev-btn"),nextArrow:$(".next-btn")});


        /*$('').countrySelect();*/



    $(".expiration-year input[type=number]").stepper();


});